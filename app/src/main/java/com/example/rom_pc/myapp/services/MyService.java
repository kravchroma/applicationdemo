package com.example.rom_pc.myapp.services;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.example.rom_pc.myapp.DAO.CoordinateObject;

import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;

public class MyService extends Service {

    private Timer timer;

    private LocationManager locationManager;
    private boolean off = true;

    public MyService() {
    }

    @Override
    public void onCreate() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //startTimer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return super.onStartCommand(intent, flags, startId);
        }
        if(off) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    5000, 0.00001f, locationListener);
            locationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, 5000 , 0.00001f,
                    locationListener);
            off = false;
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        //stopTimer();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(locationListener);
        super.onDestroy();
    }

    private void stopTimer() {
        if(timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            CoordinateObject coordinateObject = new CoordinateObject();
            coordinateObject.setX(String.valueOf(location.getLatitude()));
            coordinateObject.setY(String.valueOf(location.getLongitude()));
            coordinateObject.setTimeStamp(location.getTime());
            coordinateObject.setTimeMilis(System.currentTimeMillis());

            realm.insert(coordinateObject);
            realm.commitTransaction();
            Toast.makeText(MyService.this,
                    "Latitude: " + String.valueOf(location.getLatitude()) + " "
                            + "Longitude: " + String.valueOf(location.getLongitude()),
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
}
