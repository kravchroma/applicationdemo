package com.example.rom_pc.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rom_pc.myapp.services.MyService;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServiceActivity extends AppCompatActivity {

    @OnClick(R.id.buttonStart)
    void onClickStartService() {
        startService(new Intent(this, MyService.class));
    }

    @OnClick(R.id.buttonStop)
    void onClickStopService() {
        stopService(new Intent(this, MyService.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        ButterKnife.bind(this);
    }
}
