package com.example.rom_pc.myapp.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.example.rom_pc.myapp.DAO.CoordinateObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ROM_PC on 31.07.2016.
 */

public class MyContentProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.example.rom.myapp.test";
    public static final String URL = "content://" + PROVIDER_NAME;


    public static final String PATCH_ASC = "coord_asc";
    public static final int CODE_ASC = 1;

    public static final String PATCH_DESC = "coord_desc";
    public static final int CODE_DESC = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, PATCH_ASC, CODE_ASC);
        uriMatcher.addURI(PROVIDER_NAME, PATCH_DESC, CODE_DESC);
    }

    @Override
    public boolean onCreate() {
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(getContext()).build();
        Realm.setDefaultConfiguration(realmConfig);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<CoordinateObject> res;
        switch (uriMatcher.match(uri)) {
            case CODE_ASC: {
                res = realm.where(CoordinateObject.class)
                        .findAllSorted(CoordinateObject.TIME_MILIS, Sort.ASCENDING);
            } break;
            case CODE_DESC: {
                res = realm.where(CoordinateObject.class)
                        .findAllSorted(CoordinateObject.TIME_MILIS, Sort.DESCENDING);
            } break;
            default: {
                res = realm.where(CoordinateObject.class).findAll();
            }
        }

        String [] culmens = new String[] {"timeMilis", "timeStamp", "x", "y"};
        MatrixCursor cursor = new MatrixCursor(culmens);
        for(CoordinateObject coordinateObject : res)
            cursor.addRow(new Object[] {
                    coordinateObject.getTimeMilis(),
                    coordinateObject.getTimeStamp(),
                    coordinateObject.getX(),
                    coordinateObject.getY()
            });
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return PROVIDER_NAME;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Toast.makeText(getContext(), "MyContentProvider, переданные данные получены!!!", Toast.LENGTH_LONG).show();
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Toast.makeText(getContext(), "MyContentProvider, запрос на удаление получен!!!", Toast.LENGTH_LONG).show();
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Toast.makeText(getContext(), "MyContentProvider, запрос на обновление данных получен!!!", Toast.LENGTH_LONG).show();
        return 0;
    }
}
