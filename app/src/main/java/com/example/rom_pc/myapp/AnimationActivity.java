package com.example.rom_pc.myapp;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AnimationActivity extends AppCompatActivity {

    @BindView(R.id.view_anim)
    View viewAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        ButterKnife.bind(this);

        Keyframe kf0 = Keyframe.ofFloat(0f, 0f);
        Keyframe kf1 = Keyframe.ofFloat(.5f, 1440f);
        Keyframe kf2 = Keyframe.ofFloat(1f, 0f);

        PropertyValuesHolder pvhRotation = PropertyValuesHolder.ofKeyframe("rotation", kf0, kf1, kf2);
        ObjectAnimator rotationAnim = ObjectAnimator.ofPropertyValuesHolder(viewAnim, pvhRotation);
        rotationAnim.setDuration(10000);
        rotationAnim.start();


        Keyframe kf11 = Keyframe.ofFloat(0f, 0f);
        Keyframe kf22 = Keyframe.ofFloat(4f, 20f);
        Keyframe kf33 = Keyframe.ofFloat(0f, 0f);

        pvhRotation = PropertyValuesHolder.ofKeyframe("scaleX", kf11, kf22, kf33);
        rotationAnim = ObjectAnimator.ofPropertyValuesHolder(viewAnim, pvhRotation);
        rotationAnim.setDuration(10000);
        rotationAnim.start();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            viewAnim.animate().setDuration(10000)
                    .translationZ(1000)
                    .translationY(100)
                    .start();
        }
    }
}
