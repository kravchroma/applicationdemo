package com.example.rom_pc.myapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.rom_pc.myapp.DAO.PersonObject;
import com.example.rom_pc.myapp.adapters.RViewAdapter;
import com.example.rom_pc.myapp.widgets.DialogAddPerson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class RealmActivity extends AppCompatActivity implements DialogAddPerson.ChengCountListener {

    @BindView(R.id.rview)
    RecyclerView recyclerView;
    private DialogAddPerson dialogAddPerson;
    private RViewAdapter rViewAdapter;

    @OnClick(R.id.button_add_person)
    void onClickAddPerson() {
        dialogAddPerson.show(getFragmentManager(), "DialogAddPerson");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realm);
        ButterKnife.bind(this);

        rViewAdapter = new RViewAdapter(findAllPersonObject());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(rViewAdapter);

        dialogAddPerson = new DialogAddPerson();
        dialogAddPerson.setChengCountListener(this);
    }

    @NonNull
    private RealmResults<PersonObject> findAllPersonObject() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(PersonObject.class).findAll();
    }

    @Override
    public void chengCount() {
        rViewAdapter.setPersonObjects(findAllPersonObject());
        rViewAdapter.notifyDataSetChanged();
    }
}
