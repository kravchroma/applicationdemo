package com.example.rom_pc.myapp;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.Button;
import android.widget.TextView;

import com.example.rom_pc.myapp.providers.MyContentProvider;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContentProviderActivity extends AppCompatActivity {

    @BindView(R.id.textCoord)
    TextView textViewCoord;

    @OnClick(R.id.button_query)
    void onClickQueryTest() {
        Uri uri = Uri.parse(MyContentProvider.URL + "/" + MyContentProvider.PATCH_DESC);
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        String s = "{\"timeMilis\", \"timeStamp\", \"x\", \"y\"}<br/>";
        while (cursor!=null && cursor.moveToNext()) {
            s = s.concat("{ " + String.valueOf(cursor.getLong(0)) + ";  " +
                    String.valueOf(cursor.getLong(1)) + ";  " +
                    cursor.getString(2) + ";  " +
                    cursor.getString(3)) + " }<br/>";
        }
        textViewCoord.setText(Html.fromHtml(s));
    }
    @OnClick(R.id.button_update)
    void onClickUpdateTest() {
        Uri uri = Uri.parse(MyContentProvider.URL + "/" + MyContentProvider.PATCH_ASC);
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        String s = "{\"timeMilis\", \"timeStamp\", \"x\", \"y\"}<br/>";
        while (cursor != null && cursor.moveToNext()) {
            s = s.concat("{ " + String.valueOf(cursor.getLong(0)) + ";  " +
                    String.valueOf(cursor.getLong(1)) + ";  " +
                    cursor.getString(2) + ";  " +
                    cursor.getString(3)) + " }<br/>";
        }
        textViewCoord.setText(Html.fromHtml(s));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_provider);
        ButterKnife.bind(this);
    }
}
