package com.example.rom_pc.myapp.DAO;

import io.realm.RealmObject;

/**
 * Created by ROM_PC on 31.07.2016.
 */

public class PersonObject extends RealmObject {

    public static final String LADT_MAME_ROW = "lastName";

    private String lastName;
    private String firstName;
    private String email;
    private String phone;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
