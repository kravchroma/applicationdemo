package com.example.rom_pc.myapp;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @OnClick(R.id.button_realm_test)
    void onClickRealmTest() {
        startActivity(new Intent(this, RealmActivity.class));
    }

    @OnClick(R.id.button_contentprovider_test)
    void onClickContentproviderTest() {
        startActivity(new Intent(this, ContentProviderActivity.class));
    }

    @OnClick(R.id.button_service_test)
    void onClickServiceTest() {
        startActivity(new Intent(this, ServiceActivity.class));
    }

    @OnClick(R.id.button_android_animation_framework_test)
    void onClickAnimationFrameworkTest() {
        startActivity(new Intent(this, AnimationActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
}
