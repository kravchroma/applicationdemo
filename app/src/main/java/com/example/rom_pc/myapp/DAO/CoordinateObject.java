package com.example.rom_pc.myapp.DAO;

import io.realm.RealmObject;

/**
 * Created by ROM_PC on 02.08.2016.
 */
public class CoordinateObject extends RealmObject {

    public final static String TIME_MILIS = "timeMilis";

    private long timeMilis;
    protected long timeStamp;
    private String x;
    private String y;

    public long getTimeMilis() {
        return timeMilis;
    }

    public void setTimeMilis(long timeMilis) {
        this.timeMilis = timeMilis;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y;
    }
}
