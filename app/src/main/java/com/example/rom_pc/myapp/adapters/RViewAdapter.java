package com.example.rom_pc.myapp.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.rom_pc.myapp.DAO.PersonObject;
import com.example.rom_pc.myapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ROM_PC on 31.07.2016.
 */

public class RViewAdapter extends RecyclerView.Adapter<RViewAdapter.PersonViewHolder> {

    private List<PersonObject> personObjects;

    public void setPersonObjects(List<PersonObject> personObjects) {
        this.personObjects = personObjects;
    }

    public RViewAdapter(List<PersonObject> personObjects) {
        this.personObjects = personObjects;
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.person_layout, parent, false);
        return new PersonViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        PersonObject person = personObjects.get(position);
        holder.tvName.setText(person.getFirstName() + " " + person.getLastName());
        holder.tvEmail.setText("E-Mail: " + person.getEmail());
        holder.tvPhone.setText("Phone: " + person.getPhone());
    }

    @Override
    public int getItemCount() {
        return personObjects.size();
    }

    static class PersonViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPhone)
        TextView tvPhone;
        @BindView(R.id.tvMail)
        TextView tvEmail;

        PersonViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
