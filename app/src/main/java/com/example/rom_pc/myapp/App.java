package com.example.rom_pc.myapp;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ROM_PC on 31.07.2016.
 */

public class App extends Application {

    @Override
    public void onCreate() {
       super.onCreate();
       RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
       Realm.setDefaultConfiguration(realmConfig);
    }
}
