package com.example.rom_pc.myapp.widgets;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.rom_pc.myapp.DAO.PersonObject;
import com.example.rom_pc.myapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by ROM_PC on 31.07.2016.
 */

public class DialogAddPerson extends DialogFragment {

    @BindView(R.id.editTextFirstName)
    EditText etFirstName;
    @BindView(R.id.editTextLostName)
    EditText etLostNAme;
    @BindView(R.id.editTextPhone)
    EditText etPhone;
    @BindView(R.id.editTextEmail)
    EditText etEmail;

    private ChengCountListener chengCountListener;

    public void setChengCountListener(ChengCountListener chengCountListener) {
        this.chengCountListener = chengCountListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = inflater.inflate(R.layout.dialog_add_layout , null);
        ButterKnife.bind(this, view);
        builder.setView(view);
        builder.setPositiveButton(R.string.dialog_add, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PersonObject personObject = new PersonObject();
                        personObject.setLastName(etLostNAme.getText().toString());
                        personObject.setFirstName(etFirstName.getText().toString());
                        personObject.setPhone(etPhone.getText().toString());
                        personObject.setEmail(etEmail.getText().toString());
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        realm.insert(personObject);
                        realm.commitTransaction();
                        if(chengCountListener != null) {
                            chengCountListener.chengCount();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null);
        return builder.create();
    }

    public interface ChengCountListener {
        void chengCount();
    }
}
